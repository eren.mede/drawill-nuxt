import firebase from 'firebase';
import 'firebase/firestore';
import 'firebase/storage';

var firebaseConfig = {
  apiKey: "AIzaSyBGge7FK5lqgpR7X-tIZ0kjf3O6v_YHBno",
  authDomain: "mdb-drawill.firebaseapp.com",
  databaseURL: "https://mdb-drawill.firebaseio.com",
  projectId: "mdb-drawill",
  storageBucket: "mdb-drawill.appspot.com",
  messagingSenderId: "460616019722",
  appId: "1:460616019722:web:fc5c0ff7f912b2cb"
};
const fb = firebase.initializeApp(firebaseConfig);
const db = firebase.firestore();

export {fb,db};